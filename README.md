# Test of Markdown TOC bug

<!-- Note: links in the Wiki don't work the same way as links from the code
view:

Given the link with the syntax [text](/h3xx/test-gitea-bug/raw/branch/main/README.md):
- Wiki = https://codeberg.org/h3xx/test-gitea-bug/h3xx/test-gitea-bug/raw/branch/main/README.md
- Code = https://codeberg.org/h3xx/test-gitea-bug/raw/branch/main/README.md

Workaround: Always supply full link URL.
-->

The bug can be seen in the Wiki representation of the Table of Contents for
this page: [wiki/README](https://codeberg.org/h3xx/test-gitea-bug/wiki/README).

:eyes: --->

Steps have been taken to assure these bugs exist in every header regardless of
level.

[View this file raw](https://codeberg.org/h3xx/test-gitea-bug/raw/branch/main/README.md)

# Non-Passing

## this header contains a :eyes: shortcode emoji
```markdown
## this header contains a :eyes: shortcode emoji
```

## this header contains $ { \sum_{f}^{F} { f * N[f] } } \over { \sum_{f}^{F} { N[f] } } $ a math equation
```markdown
## this header contains $ { \sum_{f}^{F} { f * N[f] } } \over { \sum_{f}^{F} { N[f] } } $ a math equation
```

## this header contains an inline ![My Photo](inline-image-16x16.png) image
```markdown
## this header contains an inline ![My Photo](inline-image-16x16.png) image
```

## this header contains an inline LFS ![My Photo](inline-image-16x16-lfs.png) image

*Note: LFS images render fine in the [code view](https://codeberg.org/h3xx/test-gitea-bug#this-header-contains-an-inline-lfs-my-photo-inline-image-16x16-lfs-png-image)*

```markdown
## this header contains an inline LFS ![My Photo](inline-image-16x16-lfs.png) image
```

# Passing

## this\_header\_has\_underscores
```markdown
## this\_header\_has\_underscores
```

## this\\header\\has\\double-backslashes
```markdown
## this\\header\\has\\double-backslashes
```

## this header contains an HTML character class &amp;
```markdown
## this header contains an HTML character class &amp;
```

## this header has [a link](https://example.com)
```markdown
## this header has [a link](https://example.com)
```

## this header has <a href="https://example.com">an HTML link</a>
```markdown
## this header has <a href="https://example.com">an HTML link</a>
```

## this header contains a URL https://example.com
```markdown
## this header contains a URL https://example.com
```

## this header contains <!-- I have opinions! --> a comment
```markdown
## this header contains <!-- I have opinions! --> a comment
```

# Not sure

The following TOC entries could be rendering different based on a stylistic
choice. FWIW, GitHub wiki doesn't render any header text styles or links in its
TOC.

## this header has `code()`
```markdown
## this header has `code()`
```

## this header has <tt>HTML code()</tt>
```markdown
## this header has <tt>HTML code()</tt>
```

## this _header_ has *italics*
```markdown
## this _header_ has *italics*
```

## this <em>header</em> has <i>HTML italic</i>
```markdown
## this <em>header</em> has <i>HTML italic</i>
```

## this __header__ has **bold**
```markdown
## this __header__ has **bold**
```

## this <strong>header</strong> has <b>HTML bold</b>
```markdown
## this <strong>header</strong> has <b>HTML bold</b>
```

## this header has ~~strikethru~~
```markdown
## this header has ~~strikethru~~
```
